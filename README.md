**Steps on using this repository:**

In your text editor:

1. In the terminal, type: git clone https://bitbucket.org/Prashn96/hitsongprediction_ui/src/master/
2. To access the main file containing everything, type: cd master/
3. Since the Neural network model is already saved, just need to run the UI, type: python3 finale.py
4. Once run, click on local host URL that shows up in the terminal to load the UI : ctrl/command c 127.0.0.1:5000

In the User Interface:

1. Key in the desired track name and corresponding artist onto the text boxes, denoted by the placeholder.
- The probability of it being a hit song would be displayed as you click the 'predict' button (about 2 seconds wait)

